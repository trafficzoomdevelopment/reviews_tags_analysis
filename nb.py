#!/usr/bin/env python

from sklearn.naive_bayes import GaussianNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, confusion_matrix
import numpy as np
import csv

TRAIN_FILE = "data/hateval2019_en_train.csv"
TEST_FILE = "data/hateval2019_en_test.csv"

# read the trainin data
sentences_train = []
labels_train = []
with open(TRAIN_FILE) as f:
    reader = csv.DictReader(f)
    for row in reader:
        sentences_train.append(row["text"])
        labels_train.append(eval(row["HS"]))

# vectorization
tfidf = TfidfVectorizer(ngram_range=(1,3), max_features=5000)
tfidf.fit(sentences_train)
X_train = tfidf.transform(sentences_train)
y_train = np.array(labels_train)

# read the test data
sentences_test = []
labels_test = []
with open(TEST_FILE) as f:
    reader = csv.DictReader(f)
    for row in reader:
        sentences_test.append(row["text"])
        labels_test.append(eval(row["HS"]))

# vectorization
X_test = tfidf.transform(sentences_test)
y_test = np.array(labels_test)

# training
clf = GaussianNB()
clf.fit(X_train.toarray(), y_train)

# prediction
pred = clf.predict(X_test.toarray())

# evaluation
print (confusion_matrix(y_test, pred))
print (classification_report(y_test, pred))

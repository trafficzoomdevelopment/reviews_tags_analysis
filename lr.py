#!/usr/bin/env python

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, confusion_matrix
import numpy as np
import csv
import settings

TRAIN_FILE = settings.TRAIN_FILE
TEST_FILE = settings.TEST_FILE

# read the trainin data
sentences_train = []
labels_train = []
with open(TRAIN_FILE) as f:
    reader = csv.DictReader(f)
    for row in reader:
        sentences_train.append(row[settings.REVIEW_TEXT])
        labels_train.append(eval(row["HS"]))

# vectorization
tfidf = TfidfVectorizer(ngram_range=(1,3), max_features=5000)
tfidf.fit(sentences_train)
X_train = tfidf.transform(sentences_train)
y_train = np.array(labels_train)

# read the test data
sentences_test = []
labels_test = []
with open(TEST_FILE) as f:
    reader = csv.DictReader(f)
    for row in reader:
        sentences_test.append(row["text"])
        labels_test.append(eval(row["HS"]))

# vectorization
X_test = tfidf.transform(sentences_test)
y_test = np.array(labels_test)

# training
clf = LogisticRegression()
clf.fit(X_train, y_train)

# prediction
pred = clf.predict(X_test)

# evaluation
print (confusion_matrix(y_test, pred))
print (classification_report(y_test, pred))

# ngram analysis
features = {index: ngram for ngram, index in tfidf.vocabulary_.items()}
feature_weights = {features[index]: weight for index, weight in enumerate(clf.coef_[0])}
with open("feature_weights_lr.tsv", "w") as fo:
    for ngram, weight in feature_weights.items():
        fo.write("{0}\t{1}\n".format(weight, ngram))

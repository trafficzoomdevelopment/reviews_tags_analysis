import pandas as pd
import settings
import numpy as np

# From a csv file, return two lists:
# 	. one list of sentences (review texts)
# 	. one list of labels (tags)
def get_data(csv_file_path):
	data_df = pd.read_csv(csv_file_path)
	
	reviews = data_df[settings.REVIEW_TEXT].tolist()

	tags_df = data_df[data_df.columns[2:]]

	tags = tags_df.values

	s = np.where(tags_df, ['{}, '.format(x) for x in tags_df.columns], '')
	data_df.insert(loc=2, column="tags", value=pd.Series([''.join(x).strip(', ') for x in s], index=tags_df.index))

	return reviews, tags, data_df


# Get the train data
def get_train_data():
	return get_data(settings.TRAIN_FILE)


# Get the test data
def get_test_data():
	return get_data(settings.TEST_FILE)	

import mysql.connector
import csv
import pandas as pd
import common
import settings
import time

mydb = mysql.connector.connect(
  host="13.236.180.160",
  user="scoring_reviews",
  passwd="Yw2LqoyWwHsBtCYl",
  database="scoring_reviews"
)

# Constants for tags table
TAG_ID = "tag_id"
TAG_NAME = "name"

# Constants for reviews table
ZENDESK_ID = "zendesk_id"
REVIEW_TEXT = "review_text"

# Constants for reviews_tags table
TAGS = "tags"
SUBMISSION_ID = "sumission_id"

def get_tags(mycursor):
	mycursor.execute("SELECT tag_id, name FROM `submission_builder_tags`")

	myresult = mycursor.fetchall()

	tags = {}

	for x in myresult:
		tags[x[0]] = {TAG_ID: x[0], TAG_NAME: x[1]}

	return tags


def get_reviews(mycursor):
	mycursor.execute("SELECT zendesk_id, review_text FROM `zendesk_tickets`")

	myresult = mycursor.fetchall()

	reviews = {}

	for x in myresult:
		reviews[x[0]] = {ZENDESK_ID: x[0], REVIEW_TEXT: x[1]}

	return reviews


def get_reviews_tags(mycursor):
	mycursor.execute("SELECT submission_id, zendesk_id, tags FROM `submission_builder_submissions`")

	myresult = mycursor.fetchall()

	reviews_tags = {}

	for x in myresult:
		new_tags = reviews_tags.get(x[1], {}).get(TAGS, set())
		new_tags.update(x[2].split(",") if x[2] else [])
		reviews_tags[x[1]] = {ZENDESK_ID: x[1], TAGS: new_tags}

	return reviews_tags


def get_csv_data():
	mycursor = mydb.cursor()

	all_tags = get_tags(mycursor)

	all_reviews = get_reviews(mycursor)

	all_reviews_tags = get_reviews_tags(mycursor)	

	sorted_tags_names = [all_tags[tag_id][TAG_NAME] for tag_id in sorted(all_tags.keys())]
	headers = [ZENDESK_ID, REVIEW_TEXT] + sorted_tags_names

	all_data = []
	exceptions_count = 0
	for review_tags in all_reviews_tags.values():
		# print (review_tags)
		try:
			review_with_sparse_tags = all_reviews[review_tags[ZENDESK_ID]]
		except Exception as e:
			print (e)
			exceptions_count += 1
			continue
		
		for sorted_tag_name in sorted_tags_names:
			review_with_sparse_tags[sorted_tag_name]= 1 if review_tags[TAGS] is not None and sorted_tag_name in review_tags[TAGS] else 0
		
		all_data.append(review_with_sparse_tags)

	print ("Number of data", len(all_data))
	print ("Number of exceptions", exceptions_count)

	with open("all_data.csv", "w+") as output_file:
		dict_writer = csv.DictWriter(output_file, headers)
		dict_writer.writeheader()
		dict_writer.writerows(all_data)

# Get the raw data from database
# get_csv_data()
# exit()


def print_distributions_of_tags(df):
	for column in df.columns[2:]:
		filtered_df = df.apply(lambda x: True if x[column] == 1 else False , axis=1)
		print (column, len(filtered_df[filtered_df == True].index))

def split_train_test():
	all_data_df = pd.read_csv("all_data.csv")

	# print ("--------------------------- Distribution for all ---------------------------")
	# print_distributions_of_tags(all_data_df)

	from sklearn.model_selection import train_test_split

	train, test = train_test_split(all_data_df, test_size=0.2)

	# print ("--------------------------- Distribution for test ---------------------------")
	# print_distributions_of_tags(test)

	train.to_csv("train.csv", index=False)
	test.to_csv("test.csv", index=False)

# split_train_test()

def clean_standardize():
	from nltk.corpus import stopwords
	from nltk.tokenize import word_tokenize 
	from nltk.stem import PorterStemmer

	stop_words = stopwords.words('english')

	all_data_df = pd.read_csv("all_data.csv")

	ps = PorterStemmer() 

	all_data_df[REVIEW_TEXT] = all_data_df[REVIEW_TEXT].apply(lambda x: " ".join([ps.stem(word) for word in word_tokenize(x.lower()) if word not in stop_words]))

	all_data_df.to_csv("cleaned_all_data.csv", index=False)

# clean_standardize()
# exit()

def calculate_tf_idf(n_gram_n):
	from sklearn.feature_extraction.text import TfidfVectorizer
	import re
	from nltk.util import ngrams
	import math

	all_clean_data_df = pd.read_csv("cleaned_all_data.csv")	
	sentences = [sentence for x in all_clean_data_df[REVIEW_TEXT] if x for sentence in str(x).split(".")]
	tokens_list = [[token for token in re.sub(r'[^a-zA-Z0-9\s]', ' ', sentence).split(" ") if token != ""] for sentence in sentences]
	normalized_sentences = [" ".join(tokens) for tokens in tokens_list]

	n_grams_count = {}

	for sentence in normalized_sentences:
		tokens = [token for token in sentence.split(" ") if token != ""]
		for n_gram in list(ngrams(tokens, n_gram_n)):
			concatenated_n_gram = " ".join(n_gram)
			n_grams_count[concatenated_n_gram] = n_grams_count.get(concatenated_n_gram, 0) + 1

	tf_idf = {}
	print (len(n_grams_count))
	idx = 0
	for ngram, ngram_count in n_grams_count.items():
		idx += 1
		
		# if idx > 10:
		# 	break

		tf_idf[ngram] = ngram_count * math.log(len(sentences) * 1.0 / sum([1 for tmp in normalized_sentences if ngram in tmp]), 10)
	print (tf_idf)

	with open("{}_gram_{}.csv".format(n_gram_n, time.strftime("%Y%m%d-%H%M%S")), "w+") as fout:
		fout.write("ngram, tf-idf\n")
		for ngram, ngram_tf_idf in sorted(tf_idf.items(), key=lambda x: x[1], reverse=True):
			fout.write("{},{}\n".format(ngram, ngram_tf_idf))

	# with open("{}_gram.txt".format(n_gram_n), "w+") as fout:
	# 	for sentence in normalized_sentences:
	# 		# do n-gram
	# 		tokens = [token for token in sentence.split(" ") if token != ""]

	# 		sentence_result = {}
	# 		for n_gram in list(ngrams(tokens, n_gram_n)):
	# 			concatenated_n_gram = " ".join(n_gram)
	# 			# print (concatenated_n_gram)
	# 			sentence_result[concatenated_n_gram] = sentence.count(concatenated_n_gram) * \
	# 				math.log(len(sentences) * 1.0 / sum([1 for tmp in normalized_sentences if concatenated_n_gram in tmp]), 10)

	# 		fout.write(sentence + ":\n" + str(sentence_result) + "\n\n")




calculate_tf_idf(2)
calculate_tf_idf(1)
exit()


from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, confusion_matrix
import numpy as np
from sklearn.multiclass import OneVsRestClassifier

sentences_train, labels_train, _ = common.get_train_data()

# print (len(sentences_train))
# exit()

tfidf = TfidfVectorizer(ngram_range=(1,3), max_features=5000)
tfidf.fit(sentences_train)
X_train = tfidf.transform(sentences_train)
y_train = labels_train

sentences_test, labels_test, test_df = common.get_test_data()
test_result_df = test_df[test_df.columns[:3]]
tags = list(test_df.columns[3:])

# vectorization
X_test = tfidf.transform(sentences_test)
y_test = labels_test

# training
lr = LogisticRegression()
nb = GaussianNB()
svc = SVC(kernel="linear")

clfs = [lr, nb, svc]
clfs_names = ["linear regression", "naive bayes", "support vector machine"]

for clf, clf_name in zip(clfs, clfs_names): 
	ovr_clf = OneVsRestClassifier(clf)
	ovr_clf.fit(X_train.toarray() if clf == nb else X_train, y_train)

	# prediction
	pred = ovr_clf.predict(X_test.toarray() if clf == nb else X_test)

	predicted_tags = [",".join([tag for tag, tag_pred in zip(tags, each_pred) if tag_pred]) for each_pred in pred]
	
	test_result_df[clf_name] = pd.Series(predicted_tags) 

	# evaluation
	# print (confusion_matrix(y_test, pred))
	print (classification_report(y_test, pred))


test_result_df.to_csv("test_result.csv")	
